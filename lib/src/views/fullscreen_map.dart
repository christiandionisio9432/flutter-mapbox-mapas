import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:mapbox_gl/mapbox_gl.dart';

class FullScreenMap extends StatefulWidget {
  @override
  State<FullScreenMap> createState() => _FullScreenMapState();
}

class _FullScreenMapState extends State<FullScreenMap> {
  MapboxMapController? mapController;
  final center = LatLng(37.8050546, -122.483023);

  String selectedStyle = 'mapbox://styles/cdionisio/ckv60jecb76lf14p707zwxc4p';
  final oscuroStyle = 'mapbox://styles/cdionisio/ckv60ha1l2fmr14pkm3oggs0p';
  final streetStyle = 'mapbox://styles/cdionisio/ckv60jecb76lf14p707zwxc4p';

  void _onMapCreated(MapboxMapController controller) {
    mapController = controller;
    _onStyleLoaded();
  }

  /// Adds an asset image to the currently displayed style
  Future<void> addImageFromAsset(String name, String assetName) async {
    final ByteData bytes = await rootBundle.load(assetName);
    final Uint8List list = bytes.buffer.asUint8List();
    return mapController!.addImage(name, list);
  }

  /// Adds a network image to the currently displayed style
  Future<void> addImageFromUrl(String name, Uri uri) async {
    var response = await http.get(uri);
    return mapController!.addImage(name, response.bodyBytes);
  }

  void _onStyleLoaded() {
    addImageFromAsset("assetImage", "assets/custom-icon.png");
    addImageFromUrl(
        "networkImage", Uri.parse("https://via.placeholder.com/50"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: crearMapa()),
      floatingActionButton: botonesFlotantes(),
    );
  }

  Column botonesFlotantes() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        // Simbolos
        FloatingActionButton(
          child: Icon(Icons.sentiment_dissatisfied),
          onPressed: () {
            mapController!.addSymbol(SymbolOptions(
              geometry: center,
              iconImage: 'networkImage',
              textField: 'Montaña creada aquí',
              textOffset: Offset(0, 2),
            ));
          },
        ),
        // Zoom in
        FloatingActionButton(
          child: Icon(Icons.zoom_in),
          onPressed: () {
            mapController!.animateCamera(CameraUpdate.zoomIn());
          },
        ),

        // Zoom out
        FloatingActionButton(
          child: Icon(Icons.zoom_out),
          onPressed: () {
            mapController!.animateCamera(CameraUpdate.zoomOut());
          },
        ),

        SizedBox(height: 5),

        FloatingActionButton(
          child: Icon(Icons.add_to_home_screen),
          onPressed: () {
            if (selectedStyle == oscuroStyle) {
              selectedStyle = streetStyle;
            } else {
              selectedStyle = oscuroStyle;
            }
            _onStyleLoaded();

            setState(() {});
          },
        ),
      ],
    );
  }

  MapboxMap crearMapa() {
    return MapboxMap(
      styleString: selectedStyle,
      onMapCreated: _onMapCreated,
      initialCameraPosition: CameraPosition(
        target: center,
        zoom: 14,
      ),
    );
  }
}
